# How to use

Create a vars file following this template, this example is using nginx.

```yaml
__container_name: nginx
__container_image: nginx
__container_version: 1.23.0-alpine
__container_hostname: nginx
__container_domainname: example.com
__recreate: yes
__restart_policy: always
__state: started
__container_ports:
  - 8080:80
  - 4443:443
__container_env_path: ./env
__container_env_name: env_file.env
__container_volumes:
  - /templates:/etc/nginx/templates:ro
  - /nginx-cache:/etc/cache/nginx
```

Replace the variables with what you need. There is an env file that will have all the environment variables you need for the container. The format for that is FOO=BAR. 