# How to use in playbook

Example 1.

```yaml
---

- name: mount dir
  hosts: websservers
  tasks:
    - include_role: 
        name: ansible-role-mount-dir
      vars:
        __local_mount_point: /mnt
        __local_mount_directory: backup
        __remote_mount_point: "webserver.ip.address:/mnt/remote-backup"
```

Example 2.

```yaml
---

- hosts: webservers
  roles:
    - { role: ansible-role-mount-dir, __local_mount_point: /mnt, __local_mount_directory: backup,  __remote_mount_point: "webserver.ip.address:/mnt/remote-backup"}
```